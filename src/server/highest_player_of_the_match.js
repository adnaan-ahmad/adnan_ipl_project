const highest_player_of_the_match = (matches) => {
    
    const all_years = matches.map((match) => match.season)
    const years = all_years.filter((item, index) => all_years.indexOf(item) === index)

    var result2 = {}
    for(var year of years) {
            
            const count = matches.filter(match => match.season === year).reduce((acc, match) => {
                const player_of_match = match.player_of_match
                acc[player_of_match] ? acc[player_of_match] += 1 : acc[player_of_match] = 1
                return acc
            
            }, {})
            
    var highest = Math.max(...Object.values(count))
    
    for(var counter in count) {
        
        if(count[counter] === highest) {
            result2[`${year}(${counter})`] = count[counter]
            break;
        }
        }
    
    }
    return result2
}

module.exports = highest_player_of_the_match
