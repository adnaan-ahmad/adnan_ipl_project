const fs = require('fs')                        
const csv = require("csvtojson")      
const MATCHES_FILE_PATH = '../data/matches.csv'
const DELIVERIES_FILE_PATH = '../data/deliveries.csv'
const toss_match = require('./toss_match')
const toss_match_JSON = '../output/toss_match.json'
const highest_player_of_the_match = require('./highest_player_of_the_match')
const highest_player_of_the_match_JSON = '../output/highest_player_of_the_match.json'
const strike_rate = require('./strike_rate')
const strike_rate_JSON = '../output/strike_rate.json'
const best_economy_super_overs = require('./best_economy_super_overs')
const best_economy_super_overs_JSON = '../output/best_economy_super_overs.json'
const highest_dismissal = require('./highest_dismissal')
const highest_dismissal_JSON = '../output/highest_dismissal.json'


function main() {

    csv().fromFile(MATCHES_FILE_PATH).then(matches => {     
        csv().fromFile(DELIVERIES_FILE_PATH).then(deliveries => {
            
            let result1 = toss_match(matches)
            const result1_json = { toss_match : result1 }               
            const result1_string = JSON.stringify(result1_json)         
            fs.writeFile(toss_match_JSON, result1_string, 'utf8', err => { if(err) console.log(err)})

            let result2 = highest_player_of_the_match(matches)
            const result2_json = { highest_player_of_the_match : result2 }               
            const result2_string = JSON.stringify(result2_json)         
            fs.writeFile(highest_player_of_the_match_JSON, result2_string, 'utf8', err => { if(err) console.log(err)})
        
            let result3 = strike_rate(matches, deliveries)
            const result3_json = { strike_rate_Virat_Kohli : result3 }               
            const result3_string = JSON.stringify(result3_json)         
            fs.writeFile(strike_rate_JSON, result3_string, 'utf8', err => { if(err) console.log(err)})

            let result4 = best_economy_super_overs(deliveries)
            const result4_json = { best_economy_super_overs : result4 }               
            const result4_string = JSON.stringify(result4_json)         
            fs.writeFile(best_economy_super_overs_JSON, result4_string, 'utf8', err => { if(err) console.log(err)})

            let result5 = highest_dismissal(deliveries)
            const result5_json = { highest_dismissal_Virat_Kohli: result5 }               
            const result5_string = JSON.stringify(result5_json)         
            fs.writeFile(highest_dismissal_JSON, result5_string, 'utf8', err => { if(err) console.log(err)})
        
        })
    })
}

main()
