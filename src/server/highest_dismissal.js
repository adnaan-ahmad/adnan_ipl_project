const highest_dismissal = (deliveries) => {
    
    var result5 = {}

    const bowlers = deliveries.filter(delivery => delivery.player_dismissed === 'V Kohli').reduce((acc, delivery) => {
        const bowler = delivery.bowler
        acc[bowler] ? acc[bowler] += 1 : acc[bowler] = 1
        return acc
    
    }, {})

var highest_wicket = Math.max(...Object.values(bowlers))

for(var bowler in bowlers) {
    if(bowlers[bowler]  === highest_wicket) {
        result5[bowler] = `${highest_wicket} times`
    }
}

return result5
}

module.exports = highest_dismissal
