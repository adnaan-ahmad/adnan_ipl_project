const toss_match = (matches) => {

const result1 = matches.filter(match => match.toss_winner === match.winner).reduce((acc, match) => {
    const winner = match.winner
    acc[winner] ? acc[winner] += 1 : acc[winner] = 1
    return acc

}, {})
return result1
}

module.exports = toss_match
