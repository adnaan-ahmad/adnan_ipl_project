function best_economy_super_overs(deliveries) {

    const super_over_filter = deliveries.filter(delivery => delivery.is_super_over === '1')
    
    const runs = super_over_filter.reduce((acc, delivery) => {
        const bowler = delivery.bowler, total_runs = delivery.total_runs
        acc[bowler] ? acc[bowler] += parseInt(total_runs) : acc[bowler] = parseInt(total_runs)
        return acc
    }, {})
    
    const balls = super_over_filter.reduce((acc, delivery) => {
        const bowler = delivery.bowler
        acc[bowler] ? acc[bowler] += 1 : acc[bowler] = 1
        return acc
    }, {})


    let economies = {}

    for(ball in balls){
    
        if(!(economies[ball])){
            economies[ball] = runs[ball] / (balls[ball]/6)
        }
    }

    var best_economy = Math.min(...Object.values(economies))

    var result4 = {}
    for(let economy in economies) {
        if(economies[economy] === best_economy) result4[economy] = best_economy.toFixed(2)
    }
    
    return result4
}

module.exports = best_economy_super_overs
