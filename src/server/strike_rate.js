const strike_rate = (matches, deliveries) => {
    
    const all_years = matches.map((match) => match.season)
    const years = all_years.filter((item, index) => all_years.indexOf(item) === index)
    
    var result3 = {}
    for(let year of years) {

    const ids = matches.filter(match => match.season === year).map((match) => match.id)

        let first_id = Math.min(...ids)
        let last_id = Math.max(...ids)

    const filter_id_batsman = deliveries.filter(delivery => delivery.match_id >= first_id && delivery.match_id <= last_id && delivery.batsman === 'V Kohli')
    const runs = filter_id_batsman.reduce((acc, delivery) => acc += Number(delivery.batsman_runs), 0)
    const balls = filter_id_batsman.reduce((acc) => acc += 1, 0)

    var strike_rate = Number((runs/balls * 100).toFixed(2))

    result3[year] = strike_rate
    }
    return result3
}

module.exports = strike_rate
