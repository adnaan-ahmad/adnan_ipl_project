
    fetch('./strike_rate.json')
    .then(response => response.json())
    .then(data => visualize_data(data.strike_rate_Virat_Kohli, 'Strike rate of Virat Kohli', 'Per Year', 'Years', 'Strike-Rate'));

    fetch('./toss_match.json')
    .then(response => response.json())
    .then(data => visualize_data(data.toss_match, 'Toss As Well As Match Wins', 'For Each Team', 'Teams', 'Wins'));

    fetch('./highest_player_of_the_match.json')
    .then(response => response.json())
    .then(data => visualize_data(data.highest_player_of_the_match, 'Highest Player of the Match', 'Per Year', 'Years And Players', 'No of Times'))


const visualize_data = (seriesData, container, subtitle, xAxis_title, yAxis_title) => {

  Highcharts.chart(container, {
    chart: {
      type: "column"
    },
    title: {
      text: container
    },
    subtitle: {
      text:
      subtitle
    },
    xAxis: {
      type: "category",
      title: {
        text: xAxis_title
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: yAxis_title
      }
    },
    legend: {
      enabled: false
    },
    series: [
      {
        name: yAxis_title,
        data: _.pairs(seriesData)
      }
    ]
  });
}
